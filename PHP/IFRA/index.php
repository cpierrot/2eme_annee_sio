
<?php 
session_start();
require 'fonctions/monLoader.php';
?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta  charset="utf-8" />
		<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
	<title>IFRA </title>
		<style type="text/css">
			@import url(styles/ifra.css);
		</style>
	
	</head>
	<body >
		<?php
			require_once 'controleur/controleurPrincipal.php';
		?>
	</body>
</html>