<?php
class Compte {
    use Hydrate;
    private $idCompte;
    private $login;
    private $mdp;
    private $statut;
    
    public function __construct($idCompte = NULL, $login = NULL, $mdp = NULL, $statut = NULL){
        $this->idCompte = $idCompte;
        $this->login = $login;
        $this->mdp = $mdp;
        $this->nom = $statut;
    }
    public function getIdCompte()
    {
        return $this->idCompte;
    }
    
    public function setIdCompte($idCompte)
    {
        $this->login = $idCompte;
    }
    public function getStatut()
    {
        return $this->statut;
    }
    
    public function setStatut($statut)
    {
        $this->login = $statut;
    }
    
    public function getLogin()
    {
        return $this->login;
    }
    
    public function setLogin($login)
    {
        $this->login = $login;
    }
    
    public function getMdp()
    {
        return $this->mdp;
    }
    
    public function setMdp($mdp)
    {
        $this->mdp = $mdp;
    }
}