<?php
class Plante{
    use Hydrate;
    private $idPlante;
    private $nomPlante;
    private $descriptionPlante;
    
    public function __construct($unidPlante = NULL , $unnomPlante = NULL, $unedescriptionPlante = NULL){
        $this->idPlante = $unidPlante;
        $this->nomPlante = $unnomPlante;
        $this->descriptionPlante = $unedescriptionPlante;
    }

    public function getIdPlante()
    {
        return $this->idPlante;
    }

    public function getNomPlante()
    {
        return $this->nomPlante;
    }

    public function getDescriptionPlante()
    {
        return $this->descriptionPlante;
    }

    public function setIdPlante($idPlante)
    {
        $this->idPlante = $idPlante;
    }

    public function setNomPlante($nomPlante)
    {
        $this->nomPlante = $nomPlante;
    }

    public function setDescriptionPlante($descriptionPlante)
    {
        $this->descriptionPlante = $descriptionPlante;
    }

    
}