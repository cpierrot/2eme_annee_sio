<?php
class Compte{
    
    private $lesComptes = [];
    
    public function _construct($array){
        if (is_array($array)){
            $this->$lesComptes = $array;
        }
    }
    
    
    public function chercheCompte($unId){
        $i =0;
        while ($unId != $this->lesComptes[$i]->getIdCompte() && $i < count($this->lesComptes)-1){
            $i++;
        }
        if($unId == $this->lesComptes[$i]->getIdCompte()){
            return $this->lesComptes[$i];
        }
    }
    public function getlesComptes()
    {
        return $this->lesComptes;
    }
    
    public function setlesComptes($lesComptes)
    {
        $this->lesComptes = $lesComptes;
    }
    
}