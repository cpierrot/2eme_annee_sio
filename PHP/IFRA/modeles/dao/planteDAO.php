<?php
class PlanteDAO{
    
    
    // recupere toutes les plantes
    public static function getPlantes()
    {
        $result = [];
        $sql = "select * from PLANTE ";
        $liste = DBConnex::getInstance()->query($sql);
        $liste = $liste->fetchAll(PDO::FETCH_ASSOC);
        if(count($liste)> 0){
            foreach($liste as $plante){
                $objet = new Plante();
                $objet->hydrate($plante);
                $result[] = $objet;
            }
        }
        return $result;
    }
    
    //Ajoute une plante
    public static function ajouterPlante(Plante $unePlante){
        $sql = "insert into PLANTE values
            (NULL,
             '".$unePlante->getNomPlante()."',
             '".$unePlante->getDescriptionPlante()."',
                '')";
        $req = dBConnex::getInstance()->prepare($sql);
        echo $sql;
        $req->execute();
        return $req->fetch();
    }
    
    //Supprime une plante
    public static function supprimerPlante(Plante $unePlante)
    {
        $sql = "";
        $req = dBConnex::getInstance()->prepare($sql);
        $id = $unePlante->getIdPlante();
        
        $req->execute();
        return $req->fetch();
    }
    
    //Modifie une plante
    public static function modifierPlante(Plante $unePlante){
        $sql = "";
        $req = dBConnex::getInstance()->prepare($sql);
        $id = $unePlante->getIdPlante();
        
        $req->execute();
        return $req->fetch();
    }
    
    public static function getPlanteById($idPlante){
        $sql = "select * from plante where idPlante = :idPlante";
        $req = dBConnex::getInstance()->prepare($sql);
        $req->bindParam(":idPlante", $idPlante);
        $req->execute();
        return $req->fetch();
    }
}