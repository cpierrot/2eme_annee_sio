<?php

class CompteDAO{

    public static function identification(Compte $unCompte)
    {
        $sql = "select idCompte,statut,login from COMPTE where login = :login and mdp= :mdp";
        $req = dBConnex::getInstance()->prepare($sql);
        $login = $unCompte->getLogin();
        $mdp = $unCompte->getMdp();
        $req->bindParam(":login", $login);
        $req->bindParam(":mdp", $mdp);
        
        $req->execute();
        return $req->fetch(PDO::FETCH_ASSOC);
    }



    // recupere tout les comptes
    public static function getUtilisateurs()
    {        
    	$result = [];
        $sql = "select * from COMPTE "; 
        $liste = DBConnex::getInstance()->query($sql); 
		$liste = $liste->fetchAll(PDO::FETCH_ASSOC);
		if(!empty($liste)){
            foreach($liste as $utilisateur){
            	$objet = new Compte();
            	$objet->hydrate($utilisateur);
            	$result[] = $objet;            	
            }
        }
        return $result;  
    }

    public static function maxID()
    {
        $sql = "select MAX(idCompte) from compte";
        $req = dBConnex::getInstance()->prepare($sql);
        $req->execute();
        return $req->fetch();
    }

    public static function ajouterCompte(Compte $unCompte){
    	$sql = "insert into compte (idCompte, login, mdp, statut) values (:id, :login, :mdp, :statut);";
        $req = dBConnex::getInstance()->prepare($sql);
        $id = $unCompte->getIdCompte();
        $login = $unCompte->getLogin();
        $mdp = $unCompte->getMdp();
        $codeStatut = $unCompte->getStatut();
        $req->bindParam(":id", $id);
    	$req->bindParam(":login", $login);
        $req->bindParam(":mdp", $mdp);
        $req->bindParam(":statut", $statut);
        
        $req->execute();
        return $req->fetch();
    }
    
    //Supprime un utilisateur
    public static function supprimerCompte(Compte $unCompte)
    {
        $sql = "delete from compte where idCompte = :id";
        $req = dBConnex::getInstance()->prepare($sql);
        $id = $unCompte->getIdCompte();
        $req->bindParam(":id", $id);
        $req->execute();
        return $req->fetch();
    }


    public static function modifierCompte(Compte $unCompte){
    	$sql = "update compte SET login = :login, mdp = :mdp, statut = :statut WHERE idCompte = :id";
        $req = dBConnex::getInstance()->prepare($sql);
        $id = $unCompte->getIdCompte();
        $login = $unCompte->getLogin();
        $mdp = $unCompte->getMdp();
        $statut = $unCompte->getStatut();
        $req->bindParam(":id", $id);
        $req->bindParam(":login", $login);
        $req->bindParam(":mdp", $mdp);
        $req->bindParam(":statut", $statut);
        
        $req->execute();
        return $req->fetch();
    }
}
