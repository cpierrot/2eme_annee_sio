<?php


//$maConnexion = connexion($dsn,$user, $pass);
$listePlantes = new Plantes(PlanteDAO::getPlantes());

if(isset($_GET['plante'])){
    $_SESSION['plante']= $_GET['plante'];
}
else
{
    if(!isset($_SESSION['plante'])){
        $_SESSION['plante']=1;
    }
}
/*******************************************
 * Menu liste Plantes
 ******************************************/
$menuPlante = new Menu("menuPlante");

foreach($listePlantes->getPlantes() as $unePlante){
    $menuPlante->ajouterComposant($menuPlante->creerItemLien($unePlante->getIdPlante(),$unePlante->getNomPlante()));
}
$leMenuPlante = $menuPlante->creerMenu($_SESSION['plante'], "ifra=plante&plante");


/*******************************************
 * Formulaire informations Plantes
 ******************************************/
$laPlante =  $listePlantes->cherchePlante($_SESSION['plante']);

if($_SESSION['plante'] != '0'){
    $formulairePlante = new Formulaire('POST', 'index.php', 'formuPlante', 'formuPlante');
    
    $composant = $formulairePlante->creerLabelFor('nomPlante', 'Nom :');
    $formulairePlante->ajouterComposantLigne($formulairePlante->creerInputTexte("Nom :", "nomPlante",   $laPlante->getNomPlante(),1, "",1));
    $formulairePlante->ajouterComposantTab();
    $composant = $formulairePlante->creerLabelFor('description', 'Informations :');
    $formulairePlante->ajouterComposantLigne($formulairePlante->creerInputTexte("description :", "description",   $laPlante->getDescriptionPlante(),1, "",1));
    $formulairePlante->ajouterComposantTab();
    
   /* $composant = $formulaireEquip->creerLabelFor('nomEquipeLong', 'Ville :');
    //$formulaireEquip->ajouterComposantLigne($composant, 1);
    if($_SESSION['identification']!=NULL){
        $formulaireEquip->ajouterComposantLigne($formulaireEquip->creerInputTexte("nomEquipe", "nomEquipe", "Ville :",  $uneEquipe->getNomEquipe(),1,"", 1), 1);
        
    }else{
        $formulaireEquip->ajouterComposantLigne($formulaireEquip->creerInputTexte("nomEquipe", "nomEquipe", "Ville :",  $uneEquipe->getNomEquipe(),1,"", 1), 1);
        
    }
    $formulaireEquip->ajouterComposantTab();
    
    $composant = $formulaireEquip->creerLabelFor('nomEntraineur', 'Entraineur :');
    //$formulaireEquip->ajouterComposantLigne($composant, 1);
    if($_SESSION['identification']!=NULL){
        $formulaireEquip->ajouterComposantLigne($formulaireEquip->creerInputTexte("nomEntraineur", "nomEntraineur", "Entraineur :", $uneEquipe->getnomEntraineur(),1,"", 0), 1);
        
    }else{
        $formulaireEquip->ajouterComposantLigne($formulaireEquip->creerInputTexte("nomEntraineur", "nomEntraineur", "Entraineur :", $uneEquipe->getnomEntraineur(),1,"", 1), 1);
        
    }
    $formulaireEquip->ajouterComposantTab();
    
    
    $composant = $formulaireEquip->creerLabelFor('nomPresident', 'President :');
    //$formulaireEquip->ajouterComposantLigne($composant, 1);
    if($_SESSION['identification']!=NULL){
        $formulaireEquip->ajouterComposantLigne($formulaireEquip->creerInputTexte("nomPresident", "nomPresident", "President :", $uneEquipe->getNomPresident(),1,"", 0), 1);
    }else{
        $formulaireEquip->ajouterComposantLigne($formulaireEquip->creerInputTexte("nomPresident", "nomPresident", "President :", $uneEquipe->getNomPresident(),1,"", 1), 1);
    }
    
    $formulaireEquip->ajouterComposantTab();
    
    $composant = $formulaireEquip->creerLabelFor('dateFondation', 'Fondation :');
    //$formulaireEquip->ajouterComposantLigne($composant, 1);
    if($_SESSION['identification']!=NULL){
        $formulaireEquip->ajouterComposantLigne($formulaireEquip->creerInputTexte("dateFondation", "dateFondation", "Fondation :", $uneEquipe->getdateFondation(),1,"", 0), 1);
        
    }else{
        $formulaireEquip->ajouterComposantLigne($formulaireEquip->creerInputTexte("dateFondation", "dateFondation", "Fondation :", $uneEquipe->getdateFondation(),1,"", 1), 1);
     */   
    }
    $formulairePlante->creerFormulaire();


require_once "vues/plantes.php";