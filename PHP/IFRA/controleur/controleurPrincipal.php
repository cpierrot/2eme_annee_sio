<?php 

    
    
    // Authentification
    if(isset($_POST['login'])){
      
            $unIntervenant = new Compte("",$_POST['login'],$_POST['mdp']);
            $_SESSION["testAuthentification"] = CompteDAO::identification($unIntervenant);
            $_SESSION['ifra']="accueil";
     }
     else{
            $message =  "Identifiant et/ou mot de passe incorrect.";
            $_SESSION['ifra']="connexion";
     }
    
    
    // Deconnexion
    if(isset($_GET['ifra']) && isset($_SESSION['testAuthentification'])){
        if($_GET['ifra'] == 'connexion'){
            session_destroy();
            $_SESSION['testAuthentification']= 0;
            $_SESSION['ifra']="accueil";
            
        }
    }


    if(isset($_GET['ifra'])){
        $_SESSION['ifra']= $_GET['ifra'];
    }
    else
    {
        if(!isset($_SESSION['ifra'])){
            $_SESSION['ifra']="accueil";
        }
    }
    

    $menuPrincipal = new Menu("ifra");
    $menuPrincipal->ajouterComposant($menuPrincipal->creerItemLien("accueil", "Accueil"));
    $menuPrincipal->ajouterComposant($menuPrincipal->creerItemLien("plantes", "Plantes"));
    $menuPrincipal->ajouterComposant($menuPrincipal->creerItemLien("maladies", "Maladies"));
    $menuPrincipal->ajouterComposant($menuPrincipal->creerItemLien("ravageurs", "Ravageurs"));
    $menuPrincipal->ajouterComposant($menuPrincipal->creerItemLien("traitements", "Traitements"));
    $menuPrincipal->ajouterComposant($menuPrincipal->creerItemLien("observations", "Observations"));
    
    if(isset($_SESSION['testAuthentification'])){
        if($_SESSION['testAuthentification']['statut']=='drh'){
             $menuPrincipal->ajouterComposant($menuPrincipal->creerItemLien("compte", "Les comptes"));
             $menuPrincipal->ajouterComposant($menuPrincipal->creerItemLien("connexion", "Déconnexion"));
        }
        
        
    }
    else{
        $menuPrincipal->ajouterComposant($menuPrincipal->creerItemLien("connexion", "Connexion"));
    }
            /*if(isset($_SESSION['intervenant'])){
                if($_SESSION['intervenant']->getCodeStatut() != "S01" ){
                    $menuPrincipal->ajouterComposant($menuPrincipal->creerItemLien("compte", "Mon compte"));
                }
                else{
                  
                }
            }
            
        }
        else{
           
        }
    }
    else{
        $menuPrincipal->ajouterComposant($menuPrincipal->creerItemLien("connexion", "Connexion"));
        
        
       
    }
    */
    $monMenu = $menuPrincipal->creerMenu($_SESSION['ifra'], "ifra");
   
    
    include_once dispatcher::dispatch($_SESSION['ifra']);
?>