<?php
/*
 * Création du formulaire de connexion
 */
$formulaireConnexion = new Formulaire('post', 'index.php', 'connexion', 'connexion');
$formulaireConnexion->ajouterComposantLigne($formulaireConnexion->creerTitre('Connexion'));
$formulaireConnexion->ajouterComposantTab();
$formulaireConnexion->ajouterComposantLigne($formulaireConnexion->creerLabel('Login :'));
$formulaireConnexion->ajouterComposantTab();
$formulaireConnexion->ajouterComposantLigne($formulaireConnexion->creerInputTexte('login', 'login', '', 1, ' Entrez votre identifiant', ''));
$formulaireConnexion->ajouterComposantTab();
$formulaireConnexion->ajouterComposantLigne($formulaireConnexion->creerLabel('Mot de Passe :'));
$formulaireConnexion->ajouterComposantTab();
$formulaireConnexion->ajouterComposantLigne($formulaireConnexion->creerInputMdp('mdp', 'mdp',  1, ' Entrez votre mot de passe', ''));
$formulaireConnexion->ajouterComposantTab();
$formulaireConnexion->ajouterComposantLigne($formulaireConnexion-> creerInputSubmit('submitConnex', 'submitConnex', 'Se connecter'));
$formulaireConnexion->ajouterComposantTab();

$formulaireConnexion->ajouterComposantTab();

$formulaireConnexion->creerFormulaire();




include_once 'vues/visiteurs/vueConnexion.php';

?>