<!DOCTYPE html>
<html lang="fr">
<head>
	<meta  charset="utf-8" />
	<title>Plantes </title>
	<link href="styles/ifra.css" rel="stylesheet" type="text/css">

</head>

<body>

<div id="conteneur">
<?php
include 'vues/haut.php';

?>


<div id="corps">
<h1>Plantes</h1>

<?php 
include 'vues/plantes/gauchePlantes.php';
?>

<?php 
include 'vues/plantes/droitePlantes.php';
?>		
</div>
</body>
<?php
include 'vues/bas.php';

?>
</html>