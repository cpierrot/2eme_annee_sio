<?php
class EquipeDAO{
    
    
    public static function lire(Equipe $equipe){
  
        $requetePrepa = DBConnex::getInstance()->prepare("select * from Equipe where idEquipe = :idEquipe");
        $requetePrepa->bindParam( ":idEquipe", $equipe->getIdEquipe());       
        $requetePrepa->execute();
        return  $requetePrepa->fetch();   
    }
    
    
    public static function supprimer(Equipe $equipe){
        $requetePrepa = DBConnex::getInstance()->prepare("delete from equipe where idEquipe = :idEquipe");
        $idEquipe = $equipe->getIdEquipe();
        $requetePrepa->bindParam( ":idEquipe", $idEquipe);
        return $requetePrepa->execute();
    }
    
       
    
    public static function modifier(Equipe $equipe){
        $requetePrepa = DBConnex::getInstance()->prepare("update equipe set nomEquipe = :nomEquipe, nomEquipeLong = :nomEquipeLong, nomEntraineur = :nomEntraineur, nomPresident = :nomPresident, dateFondation = :dateFondation where idEquipe = :idEquipe");
      
        $idEquipe = $equipe->getIdEquipe();
        $nomEquipeLong = $equipe->getNomEquipeLong();
        $nomEntraineur = $equipe->getnomEntraineur();
        $nomEquipe = $equipe->getNomEquipe();
        $dateFondation = $equipe->getdateFondation();
        $nomPresident = $equipe->getnomPresident();
        $requetePrepa->bindParam( ":idEquipe", $idEquipe);
        $requetePrepa->bindParam( ":nomEquipe", $nomEquipe);
        $requetePrepa->bindParam( ":nomEquipeLong", $nomEquipeLong);
        $requetePrepa->bindParam( ":nomEntraineur", $nomEntraineur);
        $requetePrepa->bindParam( ":nomPresident", $nomPresident);
        $requetePrepa->bindParam( ":dateFondation", $dateFondation);
        return $requetePrepa->execute();  
        
    }
    
    public static function ajouter(Equipe $equipe){
        $requetePrepa = DBConnex::getInstance()->prepare("insert into equipe values(:idEquipe, :nomEquipe,:nomEquipeLong,:nomEntraineur, :nomPresident, :dateFondation)");
        
        $idEquipe = $equipe->getIdEquipe();
        $nomEquipeLong = $equipe->getNomEquipeLong();
        $nomEntraineur = $equipe->getnomEntraineur();
        $nomEquipe = $equipe->getNomEquipe();
        $dateFondation = $equipe->getdateFondation();
        $nomPresident = $equipe->getnomPresident();
        $requetePrepa->bindParam( ":idEquipe", $idEquipe);
        $requetePrepa->bindParam( ":nomEquipe", $nomEquipe);
        $requetePrepa->bindParam( ":nomEquipeLong", $nomEquipeLong);
        $requetePrepa->bindParam( ":nomEntraineur", $nomEntraineur);
        $requetePrepa->bindParam( ":nomPresident", $nomPresident);
        $requetePrepa->bindParam( ":dateFondation", $dateFondation);
        return $requetePrepa->execute(); 
    }
    
    
    
    public static function lesEquipes(){
        $result = [];
        $requetePrepa = DBConnex::getInstance()->prepare("select * from equipe order by nomEquipe " );
       
        $requetePrepa->execute();
        $liste = $requetePrepa->fetchAll(PDO::FETCH_ASSOC); 
        
        if(!empty($liste)){
            foreach($liste as $equipe){
                $uneEquipe = new Equipe();
                $uneEquipe->hydrate($equipe);
                $result[] = $uneEquipe;
            }
        }
        return $result;
    }
    
    
   
    
    public static function lesMatchs(Equipe $equipe){
        
       
    }
    
    
    public static function lesEquipesRencontres(){
         
    

    }
    
    
   
    
}
