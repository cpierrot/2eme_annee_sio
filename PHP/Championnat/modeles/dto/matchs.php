<?php
class Matchs{
    private $matchs= array();
    
    public function __construct($array){
        if (is_array($array)) {
            $this->matchs = $array;
        }
    }
    
    public function getMatchs(){
        return $this->matchs;
    }
    
    public function chercheMatch($unIdMatch){
        $i = 0;
        while ($unIdMatch != $this->matchs[$i]->getIdMatch() && $i < count($this->matchs)-1){
            $i++;
        }
        if ($unIdMatch == $this->matchs[$i]->getIdMatch()){
            return $this->matchs[$i];
        }
    }
}