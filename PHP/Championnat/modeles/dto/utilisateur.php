<?php
class Utilisateur{
    use Hydrate;
    private $login;
    private $mdp;
    
    public function __construct($login = NULL, $mdp = NULL){
        $this->login = $login;
        $this->mdp = $mdp;
    }
    /**
     * @return the $login
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @return the $mdp
     */
    public function getMdp()
    {
        return $this->mdp;
    }

    /**
     * @param string $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @param string $mdp
     */
    public function setMdp($mdp)
    {
        $this->mdp = $mdp;
    }

    
    
}