<?php
class Match{
    const GAGNE = 3;
    const PERDU = 0;
    const NUL = 1;
    
    use Hydrate;
    private $idMatch;
    private $journee;
    private $idEquipe1;
    private $idEquipe2;
    private $butsEquipe1;
    private $butsEquipe2;
    
    
    public function __construct($idMatch=null,$journee=null,$idEquipe1=null,$idEquipe2=null,$butsEquipe1=null,$butsEquipe2=null){
        $this->idMatch = $idMatch;
        $this->journee = $journee;
        $this->idEquipe1 = $idEquipe1;
        $this->idEquipe2 = $idEquipe2;
        $this->butsEquipe1 = $butsEquipe1;
        $this->butsEquipe2 = $butsEquipe2;
    }
    
    public function equipeGagnante(){
        if($this->butsEquipe1 > $this->butsEquipe2){
            return $this->idEquipe1;
        }
        if ($this->butsEquipe1 < $this->butsEquipe2){
            return $this->idEquipe2;
        }
        return 0;
    }
    
    public function equipePerdante(){
        if ($this->butsEquipe1 < $this->butsEquipe2){
            return $this->idEquipe1;
        }
        if ($this->butsEquipe1 > $this->butsEquipe2){
            return $this->idEquipe2;
        }
        return 0;
    }
    
    
    public function pointsGagnes($unIdEquipe){
        if ($this->idEquipe1 == $unIdEquipe or $this->idEquipe2 == $unIdEquipe){
            if ($this->equipeGagnante() ==  $unIdEquipe){
                return 3;
            }
            if ($this->equipePerdante() ==  $unIdEquipe){
                return 0;
            }
            return 1;
        }
    }
    
    
    /**
     * @return the $idMatch
     */
    public function getIdMatch()
    {
        return $this->idMatch;
    }

    /**
     * @return the $journee
     */
    public function getJournee()
    {
        return $this->journee;
    }

    /**
     * @return the $idEquipe1
     */
    public function getIdEquipe1()
    {
        return $this->idEquipe1;
    }

    /**
     * @return the $idEquipe2
     */
    public function getIdEquipe2()
    {
        return $this->idEquipe2;
    }

    /**
     * @return the $butsEquipe1
     */
    public function getButsEquipe1()
    {
        return $this->butsEquipe1;
    }

    /**
     * @return the $butsEquipe2
     */
    public function getButsEquipe2()
    {
        return $this->butsEquipe2;
    }

    /**
     * @param field_type $idMatch
     */
    public function setIdMatch($idMatch)
    {
        $this->idMatch = $idMatch;
    }

    /**
     * @param field_type $journee
     */
    public function setJournee($journee)
    {
        $this->journee = $journee;
    }

    /**
     * @param field_type $idEquipe1
     */
    public function setIdEquipe1($idEquipe1)
    {
        $this->idEquipe1 = $idEquipe1;
    }

    /**
     * @param field_type $idEquipe2
     */
    public function setIdEquipe2($idEquipe2)
    {
        $this->idEquipe2 = $idEquipe2;
    }

    /**
     * @param field_type $butsEquipe1
     */
    public function setButsEquipe1($butsEquipe1)
    {
        $this->butsEquipe1 = $butsEquipe1;
    }

    /**
     * @param field_type $butsEquipe2
     */
    public function setButsEquipe2($butsEquipe2)
    {
        $this->butsEquipe2 = $butsEquipe2;
    }

    
    
}