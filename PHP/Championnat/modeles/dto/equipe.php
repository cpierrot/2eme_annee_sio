<?php
class Equipe{
    use Hydrate;
	private $idEquipe;
	private $nomEquipe;
	private $nomEquipeLong;
	private $nomEntraineur;
	private $nomPresident;
	private $dateFondation;
	private $lesMatchs = [];

	public function __construct($unIdEquipe = NULL , $unNomEquipe = NULL, $nomEquipeLong = NULL, $nomEntraineur = NULL, $nomPresident = NULL, $dateFondation = NULL){
		$this->idEquipe = $unIdEquipe;
		$this->nomEquipe = $unNomEquipe;
		$this->nomEquipeLong = $nomEquipeLong;
		$this->	nomEntraineur = $nomEntraineur;
		$this->nomPresident = $nomPresident;
		$this->dateFondation = $dateFondation;
	}
	
	public function score(Matchs $lesMatchs){
	    $score = 0;
	    foreach($lesMatchs->getMatchs() as $match){
	        $score += $match->pointsGagnes($this->idEquipe);
	    }
	    return $score;
	}
	
	public function matchsJoues(Matchs $lesMatchs){
	    $nbMatchs = 0;
	    foreach($lesMatchs->getMatchs() as $match){
	        if($match->getIdEquipe1() == $this->idEquipe || $match->getIdEquipe2()
	            == $this->idEquipe){
	                $nbMatchs++;
	        }
	    }
	    return $nbMatchs;
	}
	
	public function matchsGagnes(Matchs $lesMatchs){
	    $nbMatchsGagnes = 0;
	    foreach($lesMatchs->getMatchs() as $match){
	        if($match->equipeGagnante() == $this->idEquipe){
	            $nbMatchsGagnes++;
	        }
	    }
	    return $nbMatchsGagnes;
	}
	
	public function matchsPerdus(Matchs $lesMatchs){
	    $nbMatchsPerdus = 0;
	    foreach($lesMatchs->getMatchs() as $match){
	        if($match->equipePerdante() == $this->idEquipe){
	            $nbMatchsPerdus++;
	        }
	    }
	    return $nbMatchsPerdus;
	    
	}
	
	
	
	public function matchsNuls(Matchs $lesMatchs){
	    $nbMatchsNuls = 0;
	    foreach($lesMatchs->getMatchs() as $match){
	        if($match->getIdEquipe1() == $this->idEquipe || $match->getIdEquipe2()
	            == $this->idEquipe){
	                if($match->getButsEquipe1() == $match->getButsEquipe2()  ){
	                    $nbMatchsNuls++;
	                }
	        }
	    }
	    return $nbMatchsNuls;
	    
	}
	
	public function butsMarques(Matchs $lesMatchs){
	    $butsMarques = 0;
	    foreach($lesMatchs->getMatchs() as $match){
	        
	        if($match->getIdEquipe1() == $this->idEquipe){
	            $butsMarques += $match->getButsEquipe1();
	        }
	        elseif ($match->getIdEquipe2() == $this->idEquipe){
	            $butsMarques += $match->getButsEquipe2();
	        }
	    }
	    return $butsMarques;
	}
	

public function butsEncaisses(Matchs $lesMatchs){
    $butsEncaisses = 0;
    foreach($lesMatchs->getMatchs() as $match){
        
        if($match->getIdEquipe1() == $this->idEquipe){
            $butsEncaisses += $match->getButsEquipe2();
        }
        elseif ($match->getIdEquipe2() == $this->idEquipe){
            $butsEncaisses += $match->getButsEquipe1();
        }
    }
    return $butsEncaisses;
}
	

	public function getIdEquipe(){
		return $this->idEquipe;
	}
	
	public function setIdEquipe($unIdEquipe){
	    $this->idEquipe =  $unIdEquipe;
	}

	public function getNomEquipe(){
		return $this->nomEquipe;
	}
	
	public function setNomEquipe($unNomEquipe){
		$this->nomEquipe = $unNomEquipe;
	}

	public function getNomEquipeLong(){
	    return $this->nomEquipeLong;
	}
	
	public function setNomEquipeLong($nomEquipeLong){
	    $this->nomEquipeLong = $nomEquipeLong;
	}
	
	public function getnomEntraineur(){
	    return $this->nomEntraineur;
	}
	
	public function setnomEntraineur($nomEntraineur){
	    $this->nomEntraineur = $nomEntraineur;
	}
	
	public function getdateFondation(){
	    return $this->dateFondation;
	}
	
	public function setdateFondation($dateFondation){
	    $this->dateFondation = $dateFondation;
	}
	
	public function getnomPresident(){
	    return $this->nomPresident;
	}
	
	public function setnomPresident($nomPresident){
	    $this->nomPresident = $nomPresident;
	}
}