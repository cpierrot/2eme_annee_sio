<?php
class Utilisateurs{
    private $Utilisateurs= array();
    
    public function __construct($array){
        if (is_array($array)) {
            $this->Utilisateurs = $array;
        }
    }
    
    public function getUtilisateurs(){
        return $this->Utilisateurs;
    }
    
    public function chercheUtilisateur($unLogin){
        $i = 0;
        while ($unLogin != $this->Utilisateurs[$i]->getLogin() && $i < count($this->Utilisateurs)-1){
            $i++;
        }
        if ($unLogin == $this->Utilisateurs[$i]->getLogin()){
            return $this->Utilisateurs[$i];
        }
    }
}