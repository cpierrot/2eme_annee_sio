<?php
$listeEquipes = new Equipes(EquipeDAO::lesEquipes());
$listeMatchs = new Matchs(MatchDAO::lesMatchs());
$tabClassement =[];
$i=0;

foreach ($listeEquipes->getEquipes() as $equipe){
        $tabClassement[$i][0] = $equipe->score($listeMatchs);
        $tabClassement[$i][1] = $equipe->getNomEquipeLong();
        $tabClassement[$i][2] = $equipe->score($listeMatchs);
        $tabClassement[$i][3] = $equipe->matchsJoues($listeMatchs);
        $tabClassement[$i][4] = $equipe->matchsGagnes($listeMatchs);
        $tabClassement[$i][5] = $equipe->matchsPerdus($listeMatchs);
        $tabClassement[$i][6] = $equipe->matchsNuls($listeMatchs);
        $tabClassement[$i][7] = $equipe->butsMarques($listeMatchs);
        $tabClassement[$i][8] = $equipe->butsEncaisses($listeMatchs);
        $i++;
    }
   
    array_multisort($tabClassement , SORT_DESC);
    
    for($i = 0 ; $i < count($tabClassement);$i++){
        $tabClassement[$i][0] = $i+1;
    }
    
    
    $tabEquipe = new Tableau('tabClass', $tabClassement);
    $tabEquipe->setTitreTab('Classement');
    $tabEquipe->ajouterTitreCol('Position');
    $tabEquipe->ajouterTitreCol('Equipe');
    $tabEquipe->ajouterTitreCol('Score');
    $tabEquipe->ajouterTitreCol('Matchs joués');
    $tabEquipe->ajouterTitreCol('Matchs gagnés');
    $tabEquipe->ajouterTitreCol('Matchs perdus');
    $tabEquipe->ajouterTitreCol('Matchs nuls');
    $tabEquipe->ajouterTitreCol('Buts marqués');
    $tabEquipe->ajouterTitreCol('Buts encaissés');

include_once 'vues/squeletteClassement.php';