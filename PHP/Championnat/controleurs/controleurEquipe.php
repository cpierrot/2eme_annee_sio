<?php

/*****************************************************************************************************
 * Instancier un objet contenant la liste des �quipes et le conserver dans une variable de session
 *****************************************************************************************************/
//$_SESSION['listeEquipes'] = new Equipes(EquipeDAO::lesEquipes());
$listeEquipe  = new Equipes(EquipeDAO::lesEquipes());
	
/*****************************************************************************************************
 * Conserver dans une variable de session l'item actif du menu equipe
 *****************************************************************************************************/
if(isset($_GET['equipe'])){
	$_SESSION['equipe']= $_GET['equipe'];
}
else
{
	if(!isset($_SESSION['equipe'])){
		$_SESSION['equipe']=33;
		$_SESSION['identification']=NULL;
	}
}



/*****************************************************************************************************
 * Créer un menu vertical à partir de la liste des équipes
 *****************************************************************************************************/
$menuEquipe = new menu("menuEquipe");


if(isset($_POST['modifier'])){
    $newEquipe = new Equipe();
    $newEquipe->setIdEquipe($_SESSION['equipe']);
    $newEquipe->setNomEquipe($_POST['nomEquipe']);
    $newEquipe->setNomEquipeLong($_POST['nomEquipeLong']);
    $newEquipe->setnomEntraineur($_POST['nomEntraineur']);
    $newEquipe->setnomPresident($_POST['nomPresident']);
    $newEquipe->setdateFondation($_POST['dateFondation']);
    EquipeDAO::modifier($newEquipe);
    
    $listeEquipe  = new Equipes(EquipeDAO::lesEquipes());
}

if(isset($_POST['enregistrer'])){
    $newEquipe = new Equipe();
    $newEquipe->setIdEquipe(null);
    $newEquipe->setNomEquipe($_POST['nomEquipe']);
    $newEquipe->setNomEquipeLong($_POST['nomEquipeLong']);
    $newEquipe->setnomEntraineur($_POST['nomEntraineur']);
    $newEquipe->setnomPresident($_POST['nomPresident']);
    $newEquipe->setdateFondation($_POST['dateFondation']);
    EquipeDAO::ajouter($newEquipe);
    
    $listeEquipe  = new Equipes(EquipeDAO::lesEquipes());
}

if(isset($_POST['supprimer'])){
    $newEquipe = new Equipe();
    $newEquipe->setIdEquipe($_SESSION['equipe']);
    $newEquipe->setNomEquipe($_POST['nomEquipe']);
    $newEquipe->setNomEquipeLong($_POST['nomEquipeLong']);
    $newEquipe->setnomEntraineur($_POST['nomEntraineur']);
    $newEquipe->setnomPresident($_POST['nomPresident']);
    $newEquipe->setdateFondation($_POST['dateFondation']);
    EquipeDAO::supprimer($newEquipe);
    $_SESSION['equipe'] = 33;
    $listeEquipe  = new Equipes(EquipeDAO::lesEquipes());
}
if(isset($_POST['ajouter'])){
    
}


foreach ($listeEquipe->getEquipes() as $uneEquipe){
	$menuEquipe->ajouterComposant($menuEquipe->creerItemLien($uneEquipe->getNomEquipe() ,$uneEquipe->getIdEquipe()));

	}




/*****************************************************************************************************
 * Récupérer l'équipe sélectionnée
 *****************************************************************************************************/
 
    $uneEquipe =  $listeEquipe->chercheEquipe($_SESSION['equipe']);
    
    
   
    
    if($_SESSION['equipe'] != '0'){
        $formulaireEquip = new Formulaire('POST', 'index.php', 'formuEquipe', 'formuEquipe');
        
        $composant = $formulaireEquip->creerLabelFor('nomEquipe', 'Nom :');
        //$formulaireEquip->ajouterComposantLigne($composant, 1);
        if($_SESSION['identification']!=NULL){
            $formulaireEquip->ajouterComposantLigne($formulaireEquip->creerInputTexte("nomEquipeLong", "nomEquipeLong", "Nom :",  $uneEquipe->getNomEquipeLong(),1, "",0), 1);
            
        }else{
            $formulaireEquip->ajouterComposantLigne($formulaireEquip->creerInputTexte("nomEquipeLong", "nomEquipeLong", "Nom :",  $uneEquipe->getNomEquipeLong(),1, "",1), 1);
            
        }
        $formulaireEquip->ajouterComposantTab();
        
        $composant = $formulaireEquip->creerLabelFor('nomEquipeLong', 'Ville :');
        //$formulaireEquip->ajouterComposantLigne($composant, 1);
        if($_SESSION['identification']!=NULL){
            $formulaireEquip->ajouterComposantLigne($formulaireEquip->creerInputTexte("nomEquipe", "nomEquipe", "Ville :",  $uneEquipe->getNomEquipe(),1,"", 1), 1);
            
        }else{
            $formulaireEquip->ajouterComposantLigne($formulaireEquip->creerInputTexte("nomEquipe", "nomEquipe", "Ville :",  $uneEquipe->getNomEquipe(),1,"", 1), 1);
            
        }
        $formulaireEquip->ajouterComposantTab();
        
        $composant = $formulaireEquip->creerLabelFor('nomEntraineur', 'Entraineur :');
        //$formulaireEquip->ajouterComposantLigne($composant, 1);
        if($_SESSION['identification']!=NULL){
            $formulaireEquip->ajouterComposantLigne($formulaireEquip->creerInputTexte("nomEntraineur", "nomEntraineur", "Entraineur :", $uneEquipe->getnomEntraineur(),1,"", 0), 1);
            
        }else{
            $formulaireEquip->ajouterComposantLigne($formulaireEquip->creerInputTexte("nomEntraineur", "nomEntraineur", "Entraineur :", $uneEquipe->getnomEntraineur(),1,"", 1), 1);
            
        }
        $formulaireEquip->ajouterComposantTab();
        
        
        $composant = $formulaireEquip->creerLabelFor('nomPresident', 'President :');
        //$formulaireEquip->ajouterComposantLigne($composant, 1);
        if($_SESSION['identification']!=NULL){
            $formulaireEquip->ajouterComposantLigne($formulaireEquip->creerInputTexte("nomPresident", "nomPresident", "President :", $uneEquipe->getNomPresident(),1,"", 0), 1);
        }else{
            $formulaireEquip->ajouterComposantLigne($formulaireEquip->creerInputTexte("nomPresident", "nomPresident", "President :", $uneEquipe->getNomPresident(),1,"", 1), 1);
        }
      
        $formulaireEquip->ajouterComposantTab();
        
        $composant = $formulaireEquip->creerLabelFor('dateFondation', 'Fondation :');
        //$formulaireEquip->ajouterComposantLigne($composant, 1);
        if($_SESSION['identification']!=NULL){
            $formulaireEquip->ajouterComposantLigne($formulaireEquip->creerInputTexte("dateFondation", "dateFondation", "Fondation :", $uneEquipe->getdateFondation(),1,"", 0), 1);
            
        }else{
            $formulaireEquip->ajouterComposantLigne($formulaireEquip->creerInputTexte("dateFondation", "dateFondation", "Fondation :", $uneEquipe->getdateFondation(),1,"", 1), 1);
            
        }
        $formulaireEquip->ajouterComposantTab();

        /***************************************
         *  boutons supprimer/modifier/ajouter
         ***************************************/
        if($_SESSION['identification']!=NULL){
            $composant = $formulaireEquip->creerInputSubmit('supprimer', 'supprimer', 'Supprimer');
            $autreComposant = $formulaireEquip->creerInputSubmit('modifier', 'modifier', 'Modifier');
            
            $composant = $formulaireEquip->concactComposants($composant, $autreComposant);
            $autreComposant=$formulaireEquip->creerInputSubmit('ajouter', 'ajouter', 'Ajouter');
            $composant= $formulaireEquip->concactComposants($composant, $autreComposant);
            $formulaireEquip->ajouterComposantLigne($composant, 2);
            $formulaireEquip->ajouterComposantTab();
        }
        
        
        $formulaireEquip->creerFormulaire();
    }

$leMenuEquipes = $menuEquipe->creerMenuEquipe($_SESSION['equipe']);

if ($_SESSION['equipe']!=0 && isset($_POST['ajouter'])){
    $formAjout = new Formulaire('POST', 'index.php', 'formAjout', 'formAjout');
    
    $composant = $formAjout->creerLabel( 'Nom :', 'nomEquipe');
    $formAjout->ajouterComposantLigne($formAjout->creerInputTexte("nomEquipeLong", "nomEquipeLong", "Nom :", "",1,"", 0), 1);
    $formAjout->ajouterComposantTab();
    $composant = $formAjout->creerLabelFor('nomEquipe', 'Ville :');
    $formAjout->ajouterComposantLigne($formAjout->creerInputTexte("nomEquipe", "nomEquipe", "Ville :", "",1,"", 0), 1);
    $formAjout->ajouterComposantTab();
    $composant = $formAjout->creerLabelFor('nomEntraineur', 'Entraineur :');
    $formAjout->ajouterComposantLigne($formAjout->creerInputTexte("nomEntraineur", "nomEntraineur", "Entraineur :", "",1,"", 0), 1);    
    $formAjout->ajouterComposantTab();
    $composant = $formAjout->creerLabelFor('nomPresident', 'President :');
    $formAjout->ajouterComposantLigne($formAjout->creerInputTexte("nomPresident", "nomPresident", "President :", "",1,"", 0), 1);
    $formAjout->ajouterComposantTab();
    $composant = $formAjout->creerLabelFor('dateFondation', 'Fondation :');
    $formAjout->ajouterComposantLigne($formAjout->creerInputTexte("dateFondation", "dateFondation", "Fondation :", "",1,"", 0), 1);
    $formAjout->ajouterComposantTab();
    
    $composant=$formAjout->creerInputSubmit('enregistrer', 'enregistrer', 'Enregistrer');
    $formAjout->ajouterComposantLigne($composant, 1);
    $formAjout->ajouterComposantTab();
    
    $formAjout->creerFormulaire();
}






include_once 'vues/squeletteEquipe.php';