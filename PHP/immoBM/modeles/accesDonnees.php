<?php
require_once 'configs/param.php';
function connection($dsn,$user,$pass){
    try {
        $connex = new PDO($dsn,$user,$pass);
        return $connex;
    } catch (PDOException $e) {
        die("erreur de connexion : " .$e);
    }
}

function typeBiens($connex){
    $requete = "select * from typebien";
    $resultat = $connex->query($requete);
    return $resultat->fetchAll(PDO::FETCH_NUM);
}

function nomCommunes($connex){
    $requete = "select * from commune WHERE bm='1'";
    $resultat = $connex->query($requete);
    return $resultat->fetchAll(PDO::FETCH_NUM);
}

