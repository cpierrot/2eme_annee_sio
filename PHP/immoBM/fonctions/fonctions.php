<?php

//***************************************************************************************************
//	Cr�er un tableau pour les listes d�roulantes
//	En param�tre : 	$min : valeur minimum de la liste
//					$pas : pas entre les diff�rentes options de la liste
//					$compteur : nombre de lignes de la liste
//					$unit� : unit� � afficher pour chaque option
//					$max : 0 pas de ligne suppl�mentaire ; 1 ajout ligne max (+ de ...) en fin de liste
//	retourne :		Le tableau contenant les values et le contenu des options POUR PRIX
//***************************************************************************************************

function tabListeMin($min,$pas,$compteur,$unite,$max){
    $select = "<select id='PrixMin' name ='PrixMin'>";
    $tab = [];
    $compte = $min;
    for($i = 0; $i<$compteur;$i++){
        $tab[$i] = $compte;
        $select .="<option value='";
        $select.=$tab[$i]."'";
        $select.=">";
        $select.=$tab[$i]. " ". $unite ."</option>";        
        $compte = $compte + $pas;
    }
    $select.="</select>"; 
    return $select;
}

function tabListeMax($min,$pas,$compteur,$unite,$max){
    $select = "<select id='PrixMax' name ='PrixMax'>";
    $tab = [];
    $min = $pas;
    $compte = $min;
    for($i = 0; $i<$compteur-1;$i++){
        $tab[$i] = $compte;
        $select .="<option value='";
        $select.=$tab[$i]."'";
        if($max==$tab[$i]){
            $select.="selected ='selected'";
        }
        $select.=">";
        $select.=$tab[$i]. " ". $unite ."</option>";
        $compte = $compte + $pas;
    }
    $tab[$i] = $compte;
    $select .="<option value='";
    $select.=$tab[$i]."'";
    $select.=">";
    $select.="+ de " . $tab[$i]. " ". $unite ."</option>";
    $select.="</select>";
    return $select;
}

//***************************************************************************************************
//	Cr�er un tableau pour les listes d�roulantes
//	En param�tre : 	$min : valeur minimum de la liste
//					$pas : pas entre les diff�rentes options de la liste
//					$compteur : nombre de lignes de la liste
//					$unit� : unit� � afficher pour chaque option
//					$max : 0 pas de ligne suppl�mentaire ; 1 ajout ligne max (+ de ...) en fin de liste
//	retourne :		Le tableau contenant les values et le contenu des options POUR SURFACE
//***************************************************************************************************

function tabListeMinS($min,$pas,$compteur,$unite,$max){
    $select = "<select id='surfaceMin' name ='surfaceMin'>";
    $tab = [];
    $compte = $min;
    for($i = 0; $i<$compteur;$i++){
        $tab[$i] = $compte;
        $select .="<option value='";
        $select.=$tab[$i]."'";
        $select.=">";
        $select.=$tab[$i]. " ". $unite ."</option>";
        $compte = $compte + $pas;
    }
    $select.="</select>";
    return $select;
}

function tabListeMaxS($min,$pas,$compteur,$unite,$max){
    $select = "<select id='surfaceMax' name ='surfaceMax'>";
    $tab = [];
    $min = $pas;
    $compte = $min;
    for($i = 0; $i<$compteur-1;$i++){
        $tab[$i] = $compte;
        $select .="<option value='";
        $select.=$tab[$i]."'";
        $select.=">";
        $select.=$tab[$i]. " ". $unite ."</option>";
        $compte = $compte + $pas;
    }
    $tab[$i] = $compte;
    $select .="<option value='";
    $select.=$tab[$i]."'";
    $select.="selected ='selected'>";
    $select.="+ de " . $tab[$i]. " ". $unite ."</option>";
    $select.="</select>";
    return $select;
}

//***************************************************************************************************
//	Cr�er un tableau pour les listes d�roulantes
//	En param�tre : 	$min : valeur minimum de la liste
//					$pas : pas entre les diff�rentes options de la liste
//					$compteur : nombre de lignes de la liste
//					$unit� : unit� � afficher pour chaque option
//					$max : 0 pas de ligne suppl�mentaire ; 1 ajout ligne max (+ de ...) en fin de liste
//	retourne :		Le tableau contenant les values et le contenu des options POUR PIECES
//***************************************************************************************************

function tabListeMinP($min,$pas,$compteur,$unite,$max){
    $select = "<select id='nbPieceMin' name ='nbPieceMin'>";
    $tab = [];
    $compte = $min;
    for($i = 0; $i<$compteur;$i++){
        $tab[$i] = $compte;
        $select .="<option value='";
        $select.=$tab[$i]."'";
        $select.=">";
        $select.=$tab[$i]. " ". $unite ."</option>";
        $compte = $compte + $pas;
    }
    $select.="</select>";
    return $select;
}

function tabListeMaxP($min,$pas,$compteur,$unite,$max){
    $select = "<select id='nbPieceMax' name ='nbPieceMax'>";
    $tab = [];
    $min = $pas;
    $compte = $min;
    for($i = 0; $i<$compteur-1;$i++){
        $tab[$i] = $compte;
        $select .="<option value='";
        $select.=$tab[$i]."'";
        $select.=">";
        $select.=$tab[$i]. " ". $unite ."</option>";
        $compte = $compte + $pas;
    }
    $tab[$i] = $compte;
    $select .="<option value='";
    $select.=$tab[$i]."'";
    $select.="selected ='selected'>";
    $select.="+ de " . $tab[$i]. " ". $unite ."</option>";
    $select.="</select>";
    return $select;
}


//***************************************************************************************************
//	Ajouter une liste d�roulante dans un formulaire
//	En param�tre : 	$id : id de la balise select	
//					$name : name de la balise select
//					$tablo contient les valeurs des options
//					$selected : valeur de l'option s�lectionn�e
//	retourne une liste d�roulante.
//***************************************************************************************************
function ajouterSelect($id,$name,$tablo,$selected){
    $select = "<select id='".$id."' name ='".$name."'>";
    foreach ($tablo as $ligne){
        $select.= "<option value='";
        $select.=$ligne[0]."'";
        
        if($ligne[0]==$selected){
            $select.="selected ='selected'";
        }
        $select.=">";
        $select.=$ligne[1]."</option>";
    }
    $select.="</select>";
    return $select;
}


//***************************************************************************************************
//	Ajouter un groupe boutons radio dans un formulaire
//	En param�tre : 	$name : name du groupe
//					$tablo contient les values , ids et les labels des boutons radio
//					$colLabel : indice colonne du label
//					$colValue : indice colonne de la value
//					$colValue : indice colonne de la value
//					$selected : valeur du bouton s�lectionn�
//					$fonctionJS : appel fonction JavaScript
//	Affiche les boutons radio.
//***************************************************************************************************
function ajouterRadio($name,$tablo,$selected,$fonctionJS){
    $select="";
    for($i = 0; $i < count($tablo); $i++){
        $select.="<input type = 'radio' name='". $name. "' value='". $tablo[$i][0]."' id='". $tablo[$i][1]."'";
        
        if($selected == $tablo[$i][0]){
            $select.="checked='checked'";
        }
       
        
        $select .= $fonctionJS;
        $select .=" />";
        
        
        $select .= "<label>". $tablo[$i][2] ."</label><br/>";
    }
    return $select;
}

//***************************************************************************************************
//	Bouton checkbox
//***************************************************************************************************

function ajouterCheckbox($tablo,$name){
    $select="";
    for($i = 0; $i < count($tablo); $i++){
        $select .=      "<input type = 'checkbox' name='".$name."[]' value='".$tablo[$i][0]."' />";
        $select .=      "<label for name='".$name."'>". $tablo[$i][2] ."</label><br/>";    
    }
    return $select;
}

function  boutons(){
    $res = "";
    $res .=  "<input type ='reset' value='Annuler' class = 'bouton'/>";
    $res .=  "<input type ='submit' value='Rechercher' class = 'bouton' />";
    
    return $res;
}
//***************************************************************************************************
//	debut de requête
//***************************************************************************************************

function creerRequeteCriteres($unType, $unPrixMin, $unPrixMax, $uneSurfaceMin, $uneSurfaceMax, $unNBPiecesMin, $unNBPiecesMax, $uneLocalisation, $unTabloCommunesBM)
{
    $requete = "select distinct bien.codeBien, libelleType, prixBien, surfaceBien, nbPieces, nomCommune, codePostalCommune, nomPhoto, descriptionBien 
                from bien, typebien, commune, photo 
                where bien.codetype = typebien.codeType and bien.codeCommune = commune.codeCommune and bien.codeBien = photo.codeBien ";
    if($unType != 0){
        $requete = $requete." and bien.codeType = ".$unType;
    }
    if($unPrixMin != 0){
        $requete = $requete." and prixBien >= ". $unPrixMin;
    }
    if($unPrixMax != 50000){
        $requete = $requete." and prixBien <= ". $unPrixMax;
    }
    if($uneSurfaceMin !=10){
        $requete = $requete." and surfaceBien >= ". $uneSurfaceMin;
    }
    if($uneSurfaceMax !=200){
        $requete = $requete." and surfaceBien <= ". $uneSurfaceMax;
    }
    if($unNBPiecesMin !=1){
        $requete = $requete." and nbPieces >= ". $unNBPiecesMin;
    }
    if($unNBPiecesMax !=10){
        $requete = $requete." and nbPieces <= ". $unNBPiecesMax;
    }
    if($uneLocalisation !="indifférent"){
        if($uneLocalisation == "bm"){
            $requete = $requete."and commune.bm = '1' ";
        }
        else if($uneLocalisation == "horsbm"){
            $requete = $requete."and commune.bm = '0' ";
        }
        else if($uneLocalisation == "choixCommunes"){
            $requete = $requete."and (commune.codeCommune = ".$unTabloCommunesBM[0];
            for($i=1;$i<count($unTabloCommunesBM);$i++){
                $requete = $requete." or commune.codeCommune = ".$unTabloCommunesBM[$i];
            }
            $requete = $requete.")";
        }
    }
    return $requete;
}