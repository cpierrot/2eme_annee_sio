<?php
require_once 'modeles/accesDonnees.php';
$connex = connection($dsn, $user, $pass);

//typeBiens
if(isset($_POST['TypeBien'])){
    $typeBienSelect = $_POST['TypeBien'];
}
else{
    $typeBienSelect ="Indifférent";
}


$listeTypeBien = typeBiens($connex);
$listeDeroulanteType = ajouterSelect("TypeBien", "TypeBien", $listeTypeBien, $typeBienSelect );





//prix
if(isset($_POST['PrixMin'])){
    $prixMinselect = $_POST['PrixMin'];
}
else{
    $prixMinselect =0;
}
if(isset($_POST['PrixMax'])){
    $prixMaxselect = $_POST['PrixMax'];
}
else{
    $prixMaxselect = 1000000;
}
$listPrixMini = tabListeMin(0, 50000, 20, "&euro;", $prixMinselect);
$listPrixMax = tabListeMax(0, 50000, 20, "&euro;", $prixMaxselect);

//surface
$listSurfaceMini = tabListeMinS(10,10,19,"m&#178;",210);
$listSurfaceMax = tabListeMaxS(0,10,20,"m&#178;",210);

//nbpieces
$listPiecesMini = tabListeMinP(1,1,9,"",10);
$listPiecesMax = tabListeMaxP(1,1,10,"",10);

//localisation

$tabloLocal = array();
array_push ($tabloLocal , array("indifférent","indifférent"," Indifférent"));
array_push($tabloLocal , array( "bm","bm"," Bordeaux métropole"));
array_push($tabloLocal , array( "horsbm","horsbm"," Hors Bordeaux métropole"));
array_push($tabloLocal  , array( "choixCommunes","choixCommunes"," Choix communes BM"));


$listeDeroulanteLocal = ajouterradio("TypeLocal", $tabloLocal, 'indifférent', "onchange = 'affichebm()'");

//liste commune
$tabloNomCommunes = nomCommunes($connex);
$listeDeroulanteCommunes = ajouterCheckbox($tabloNomCommunes, "NomCommunesLocal");

//boutons
$boutons = boutons();

//Rechercher
if (isset($_POST['TypeBien'])){
        if(isset($_POST['NomCommunesLocal'])){
            $requete = creerRequeteCriteres($_POST['TypeBien'],$_POST['PrixMin'],$_POST['PrixMax'],$_POST['surfaceMin'],$_POST['surfaceMax'],$_POST['nbPieceMin'],$_POST['nbPieceMax'],$_POST['TypeLocal'],$_POST['NomCommunesLocal']);
        }
        else{
            $requete = creerRequeteCriteres($_POST['TypeBien'],$_POST['PrixMin'],$_POST['PrixMax'],$_POST['surfaceMin'],$_POST['surfaceMax'],$_POST['nbPieceMin'],$_POST['nbPieceMax'],$_POST['TypeLocal'],"");
        }
    
}