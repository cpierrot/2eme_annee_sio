
<br/>
<form method="post" action="index.php">
<!-- ****************************************************************************************************** -->
<!-- Choix du type de bien -->
<!-- ****************************************************************************************************** -->
<div class='titre'>Type de bien</div>
<div class='ligneForm'>
	<label for='choixType'>Type de bien : </label>
	<?php 
	echo $listeDeroulanteType;
	?>
</div>
	

<!-- ****************************************************************************************************** -->
<!-- Choix du prix min et max du bien recherché -->
<!-- ****************************************************************************************************** -->
<div class='titre'>Prix</div>
<div class='ligneForm'>
	<label for="choixPrixMin"> De : </label>
	<?php 
	   echo $listPrixMini;
	?>
	<label for="choixPrixMax"> &#224; : </label>
	<?php 
	   echo $listPrixMax;
	?>
</div>
<!-- ****************************************************************************************************** -->
<!-- Choix de la surface min et max du bien recherché -->
<!-- ****************************************************************************************************** -->
<div class='titre'>Surface</div>
<div class='ligneForm'>
	<label for="choixSurfaceMin"> De : </label>
	<?php 
	   echo $listSurfaceMini;
	?>
	<label for="choixSurfaceMax"> &#224; : </label>
	<?php 
	   echo $listSurfaceMax;
	?>
</div>
<!-- ****************************************************************************************************** -->
<!-- Choix du nombre de pi�ces min et max du bien recherché -->
<!-- ****************************************************************************************************** -->
<div class='titre'>Nombre de pièces</div>
<div class='ligneForm'>
	<label for="choixSurfaceMin"> De : </label>
	<?php 
	   echo $listPiecesMini;
	?>
	<label for="choixSurfaceMax"> &#224; : </label>
	<?php 
	   echo $listPiecesMax;
	?>
</div>
<!-- ****************************************************************************************************** -->
<!-- Choix localisation du bien recherché -->
<!-- ****************************************************************************************************** -->
<div class='titre'>Localisation</div>
<div class='ligneForm'>
	<div id='listeRadio'>
		<?php 
	       echo $listeDeroulanteLocal;
	    ?>
	</div>
</div>
<!-- ****************************************************************************************************** -->
<!-- Choix communes de la cub -->
<!-- ****************************************************************************************************** -->

<div class='titre' id='titreListeCommunes'>Liste des communes</div>
<div class='ligneForm'>
	<div id='listeCommunes'>
		<?php 
           echo $listeDeroulanteCommunes;
        ?>
	</div>
</div>


          
<div class='ligneForm'>
	<div id='boutons'>
		<?php 
		  echo $boutons;
		?>
	</div>
</div>

</form>
<br/>



