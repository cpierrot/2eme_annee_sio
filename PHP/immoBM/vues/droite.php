<?php 

if(isset($_SESSION['choixMenu'])){
	if($_SESSION['choixMenu']==1){
		include_once 'droiteAccueil.php';
	}
	elseif($_SESSION['choixMenu']==2){
		include_once 'droiteAchat.php';
	}
	elseif($_SESSION['choixMenu']==4){
		include_once 'droiteNosAgences.php';
	}
	elseif($_SESSION['choixMenu']==5){
		include_once 'droiteOutils.php';
	}
	elseif($_SESSION['choixMenu']==6){
		include_once 'droiteContact.php';
	}
	elseif($_SESSION['choixMenu']==7){
		include_once 'droiteMonCompte.php';
	}
}
else {
	include_once 'droiteAccueil.php';
}
